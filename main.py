from fastapi import FastAPI
import os
import psycopg2
from google.cloud import secretmanager_v1

# make sure that account has secretmanager.versions.access
# permission on the object (Secret Manager Secret Access role)
request = secretmanager_v1.AccessSecretVersionRequest(
    name="projects/743371307999/secrets/database-password/versions/1",
)

# uses default creds from "gcloud auth application-default"
client = secretmanager_v1.SecretManagerServiceClient()

response = client.access_secret_version(request=request)

# without decode it will add b'...' things...
db_password = response.payload.data.decode(encoding="utf-8")
db_user = os.environ.get("CLOUD_SQL_USERNAME")
db_name = os.environ.get("CLOUD_SQL_DATABASE_NAME")
db_connection_name = os.environ.get("CLOUD_SQL_CONNECTION_NAME")

host = f"/cloudsql/{db_connection_name}"

cnx = psycopg2.connect(
    dbname=db_name,
    user=db_user,
    password=db_password,
    host=host,
)

app: FastAPI = FastAPI()


@app.get("/")
async def root():
    with cnx.cursor() as cursor:
        cursor.execute("SELECT NOW() as now;")
        result = cursor.fetchall()
    current_time = result[0][0]
    cnx.commit()
    cnx.close()

    return {"time": str(current_time)}
